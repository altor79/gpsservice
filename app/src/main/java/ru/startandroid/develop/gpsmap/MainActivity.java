package ru.startandroid.develop.gpsmap;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import ru.startandroid.develop.gpsmap.model.MyPoint;

public class MainActivity extends FragmentActivity implements OnMapReadyCallback, View.OnClickListener, MyGpsListener {
    final String LOG_TAG = "MyLOGS";
    Intent intent;
    Button btnViber, btnGeo;
    Button onStartCommand;
  GoogleMap mMap;
    Button btnTestMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        startService(new Intent(this, MyServiceGPS.class));
        onStartCommand = findViewById(R.id.btnMap);
        onStartCommand.setOnClickListener(this);
        btnTestMap = findViewById(R.id.btnTestMap);
        btnTestMap.setOnClickListener(this);
        btnGeo = findViewById(R.id.btnGeo);
        btnGeo.setOnClickListener(this);
    }
    @Override
    public void onMapReady(GoogleMap map) {
        Log.d(LOG_TAG," ТЫ ГДЕ СУКАААА");
        mMap = map;

    }


    @Override
    protected void onResume() {
        super.onResume();
        // --- ++++ service.setvc(this)
    }

    @Override
    protected void onPause() {
        // ---<<<<< service.setvc(null)
        super.onPause();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnMap:
                intent = new Intent(this, GeoGPS.class);
                startActivity(intent);
                break;
            default:
                break;
        }
    }


    @Override
    public MyPoint getNewPoint(MyPoint myPoint) {
        // map
        return null;

        }
    }


