package ru.startandroid.develop.gpsmap;

import android.Manifest;
import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.util.Pair;

import androidx.annotation.RequiresApi;

import java.util.ArrayList;
import java.util.Date;

import ru.startandroid.develop.gpsmap.model.MyPoint;

public class MyServiceGPS extends Service implements LocationListener {
    LocationManager locationManager;

    final String LOG_TAG = "MyLOGS";
    final String LOG = "LOG Servise";

    private ArrayList<MyPoint> points = new ArrayList<>();
    private MyGpsListener myGpsListener;

    public MyGpsListener getMyGpsListener() {
        return myGpsListener;
    }

    public void setMyGpsListener(MyGpsListener myGpsListener) {
        this.myGpsListener = myGpsListener;
    }

    public ArrayList<MyPoint> getPoints() {
        return points;
    }

    // РАБОТА СЕРВИСА\
    public void onCreate() {
        super.onCreate();
        Log.d(LOG_TAG, "CОЗДАН СЕРВИС ");
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(LOG_TAG, "сервис запущен ");

        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        initLocationListener();

        return super.onStartCommand(intent, flags, startId);
    }

    public void onDestroy() {
        locationManager.removeUpdates(this);
        super.onDestroy();
        Log.d(LOG_TAG, "сервис прибит ");
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    void initLocationListener() {

        if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                1000, 1, this);
        locationManager.requestLocationUpdates(
                LocationManager.NETWORK_PROVIDER, 1000, 0,
                this);
    }

    @Override
    public void onLocationChanged(Location location) {
        showLocation(location);
        MyPoint point = new MyPoint(location.getLongitude(), location.getLatitude(), System.currentTimeMillis());
        Log.d(LOG, "location ----" + point);
//        points.add(point);
//        if (myGpsListener != null && points.size() > 0) {
//            myGpsListener.getNewPoint(points.get(points.size() - 1));
        }


    @Override
    public void onProviderDisabled(String provider) {
        checkEnabled();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onProviderEnabled(String provider) {
        checkEnabled();
        if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        showLocation(locationManager.getLastKnownLocation(provider));
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        if (provider.equals(LocationManager.GPS_PROVIDER)) {
        } else if (provider.equals(LocationManager.NETWORK_PROVIDER)) {
        }
    }

    private void showLocation(Location location) {
        if (location == null)
            return;
        if (location.getProvider().equals(LocationManager.GPS_PROVIDER)) {
        } else if (location.getProvider().equals(
                LocationManager.NETWORK_PROVIDER)) {
        }
        Log.d(LOG, "L O G " + location.getLatitude());
        Log.d(LOG, "L O G DOLGOTA" + location.getLongitude());
        Log.d(LOG_TAG, "LISTTTT" + points.toString());
    }

    private void checkEnabled() {
    }

}

