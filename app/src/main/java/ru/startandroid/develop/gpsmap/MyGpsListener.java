package ru.startandroid.develop.gpsmap;

import ru.startandroid.develop.gpsmap.model.MyPoint;

public interface MyGpsListener {
    MyPoint getNewPoint(MyPoint myPoint);
}
